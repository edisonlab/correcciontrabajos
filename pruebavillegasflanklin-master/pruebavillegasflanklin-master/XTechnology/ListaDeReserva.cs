﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XTechnology
{
    class ListaDeReserva
    {

        private List<Dispositivo> reserva = new List<Dispositivo>();

        public List<Dispositivo> Reserva
        {
            get { return reserva; }
            set { reserva = value; }
        }

        public void Agregar(Dispositivo nuevoElemento)
        {
            this.reserva.Add(nuevoElemento);
        }

        public void EnseñarDispositivoss()
        {
            foreach (Dispositivo dispositivo in Reserva)
            {
                Console.WriteLine(dispositivo.MostrarDispositivos());
            }
        }
    }
}
