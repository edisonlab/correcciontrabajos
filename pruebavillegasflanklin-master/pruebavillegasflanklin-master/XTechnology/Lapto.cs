﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XTechnology
{
    class Lapto : Dispositivo
    {
        public override string MostrarDispositivos()
        {

            return "lapto: " + Tipo + " " + Marca + " " + Modelo;
        }
    }
}
