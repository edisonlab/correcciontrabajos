﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XTechnology
{
    class Cliente
    {
        private String nombre;

        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private String apellido;

        public String Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }
        private String numeroCedula;

        public String NumeroCedula
        {
            get { return numeroCedula; }
            set { numeroCedula = value; }
        }

        public string Imprimir()
        {
            return Nombre + " " + Apellido + " " + NumeroCedula;
        }



    }
}
