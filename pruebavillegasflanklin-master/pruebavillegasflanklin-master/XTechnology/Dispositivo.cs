﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XTechnology
{
    abstract class Dispositivo
    {
        private String tipo;

        public String Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        private static String marca;

        public static String Marca
        {
            get { return marca; }
            set { marca = value; }
        }
        private String modelo;


        public abstract String MostrarDispositivos();
        public String Modelo
        {
            get { return modelo; }
            set { modelo = value; }
        }

    }
}

