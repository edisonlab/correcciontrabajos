﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XTechnology
{
    class Audifono : Dispositivo
    {
        public override string MostrarDispositivos()
        {
            return "audifono: " + Tipo + " " + Marca + " " + Modelo;
        }
    }
}
