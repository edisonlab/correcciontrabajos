﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XTechnology
{
    class Smartphone : Dispositivo
    {
        public override string MostrarDispositivos()
        {
            return " Smartphone: " + Tipo + " " + Marca + " " + Modelo;
        }
    }
}
